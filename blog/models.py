
# Create your models here.
from __future__ import unicode_literals
from django.contrib.auth.models import User

from django.db import models

class Post(models.Model):
    users= models.ForeignKey(User)
    title=models.CharField(max_length=901)
    content =models.TextField()
    img =models.ImageField(upload_to='blog.img/')#to add images
    date=models.DateTimeField(auto_now=True,auto_now_add=False)
    is_published=models.BooleanField(default=False)

    def __str__(self):
        return self.title

class Comment(models.Model):
    user=models.ForeignKey(User)
    post=models.ForeignKey(Post)
    comment_text=models.CharField(max_length=900)
    data=models.DateTimeField(auto_now=True,auto_now_add=False)

    def __str__(self):
        return self.comment_text


