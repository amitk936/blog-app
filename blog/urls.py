from django.conf.urls import url,include
from blog.views import home
from blog.views import post_detail,add_post,edit_post,delete_Post

urlpatterns =[
    url(r'^$',home,name="home"),
    url(r'^(?P<id>[0-9]+)/$',post_detail,name="post"),
    url(r'^add/$',add_post,name="add_post"),
    url(r'^(?P<pk>[0-9]+)/edit/$',edit_post,name="edit_post"),
    url(r'^(?P<id>[0-9]+)/delete',delete_Post,name="delete_post"),

    #url(r'^login/$',login,name="login")
    #url fro classes

]