from django.shortcuts import render,redirect
from django.http import HttpResponse
from django.views import generic

from question.models import Question, Choice
# Create your views here.
def index(request):
    q=Question.objects.order_by('pub_date')[:5]
    context={'question':q}
    template='question/index.html'
    return render(request,template,context)

#def question_details(request,id):
 #   q=Question.objects.get(id=int(id))
  #  context={'question':q}
   # template='question/questions.html'
    #return render(request,template,context)
class QuestiondetailView(generic.DetailView):
    model=Question
    pk_url_kwarg='id'
    template_name='question/questions.html'

class QuestionListView(generic.ListView):
    model=Choice
    template_name='question/index.html'


def question_result(request,id):
    question=Question.objects.get(id=int(id))
    choices=Choice.objects.filter(question=question)
    context={'question':question,'choices':choices}
    template='question/result.html'
    return render(request,template,context)

def question_vote(request,id):
    question = Question.objects.get(id=int(id))
    choices = Choice.objects.filter(question=question)
    context={'question':question,'choices':choices}
    template='question/votes.html'
    if request.method=='POST':
        id=int(request.POST['choice'])
        c=Choice.objects.get(id=id)
        c.vt+= 1
        c.save()
        return redirect('result',question.id)
    else:
        return render(request,template,context)




