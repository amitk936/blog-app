from django.conf.urls import url, include

from question import admin
from question import views
from question.views import QuestiondetailView

urlpatterns = [
    url(r'^$', views.index, name="home"),
    url(r'^(?P<id>[0-9]+)/$', QuestiondetailView.as_view(), name="question_detail"),
    url(r'^(?P<id>[0-9]+)/result/', views.question_result, name="result"),
    url(r'^(?P<id>[0-9]+)/vote/', views.question_vote),
    # url(r'^home/$', home, name='home'),
]
"""
    1./-list latest 5
    2./2/-show that question
    3/1/result-show the result

"""