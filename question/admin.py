from django.contrib import admin

# Register your models here.
from question.models import Question
from question.models import Choice


admin.site.register(Question)
admin.site.register(Choice)
