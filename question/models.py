from django.db import models

# Create your models here.
class Question(models.Model):
    q_text=models.CharField(max_length=200)
    pub_date=models.DateField(auto_now=True,auto_now_add=False)

    def __str__(self):#when we print this question
        return self.q_text


class Choice(models.Model):
    question=models.ForeignKey(Question,on_delete=models.CASCADE)
    choice_text=models.CharField(max_length=150)
    vt=models.IntegerField()

    def __str__(self):
        return self.choice_text
